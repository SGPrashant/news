import axios from "axios";

export const SearchItemSuccess = (data) => {
  console.log(data);
  return {
    type: "SEARCH",
    data,
  };
};

export const SearchItem = (data) => {
  console.log(data);
  return (dispatch) => {
    try {
        const url = "https://hn.algolia.com/api/v1/search?query=";
        const res=axios.get(url+data)
      res.then((res) => {
         if (res.status === 200) {
        dispatch(SearchItemSuccess(res.data));
         }
      });
    } catch (error) {
      console.log(error);
    }
  };
};

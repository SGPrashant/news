import "./App.css";
import { connect } from "react-redux";
import { useState } from "react";
import moment from "moment";
import { SearchItem } from "./redux/action";

function App(props) {
  const [input, setInput] = useState('') 

  const handelchange=(event)=>{
    setInput(event.target.value)
  }

  const handelsubmit=(event)=>{
    event.preventDefault();
    props.SearchItem(input)
    setInput("")
    
  }

  return (
    <>
      <div className="topnav">
        <h1>HackersNews</h1>
        <div className="search-container">
          <form onSubmit={handelsubmit} autoComplete="off">
            <input
              type="text"
              placeholder="Type.."
              name="search"
              onChange={handelchange}
              value={input}
            />
            <button>Search</button>
          </form>
        </div>
      </div>
      <div className="rock">
        {props.data.hits?
          props.data.hits.map((data) => {
            
            return (
              <>
                <p>
                  <b>{data.title}</b> <a href={data.url}>({data.url})</a>
                </p>
                <p>
                  {data.points + " points"} | {data.author} | posted at{" "}
                  {moment(data.created_at).format('D/MM/YYYY')} | {data.num_comments + " comments"}
                </p>
                <hr />
              </>
            );
          })
         : (
          <>
          <div className="nodata" >
            <h1>
              No Data To Show</h1></div>
              </>
        )}
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};
const mapDispatchToProps = (dispatch) => ({
  SearchItem: (inpt) => dispatch(SearchItem(inpt)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
